class User < ApplicationRecord
  attr_accessor :remember_token
  before_save { email.downcase! }
  # nameには空欄禁止、更に50文字以上の名前禁止
  validates :name, presence: true, length: { maximum: 50}
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  # emailには空欄禁止、更に255文字以上のアドレス禁止
  validates :email, presence: true, length: { maximum: 255}, 
  format: { with: VALID_EMAIL_REGEX },uniqueness: { case_sensitive: false }
  # ハッシュ化
  has_secure_password
  # 最小文字数のバリデーションと、空白を入力されないようにするバリデーション
  # また、パスワードが空のままでも更新できるようにもする
  validates :password, presence: true, length: { minimum: 6 }, allow_nil: true
  
  # 渡された文字列のハッシュ値を返す
  def self.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                    BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end
    
  # ランダムなトークンを返す
  def self.new_token
    SecureRandom.urlsafe_base64
  end
  
  # 永続セッションのためにユーザーをデータベースに記憶する
  # ランダムなトークンを更にハッシュ値にし、update_attributeメソッドを使って記憶ダイジェストを更新
  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end
  
  # 渡されたトークンがダイジェストと一致したらtrueを返す
  # 記憶ダイジェストがnilの場合falseを返す
  def authenticated?(remember_token)
    return false if remember_digest.nil?
    BCrypt::Password.new(remember_digest).is_password?(remember_token)
  end
  
  # ユーザーのログイン情報を破棄する
  def forget
    update_attribute(:remember_digest, nil)
  end
end