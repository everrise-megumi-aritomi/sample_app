class SessionsController < ApplicationController
  # ログインページを表示
  def new
  end
  
  # ログインを完了する
  def create
    @user = User.find_by(email: params[:session][:email].downcase)
    if @user && @user.authenticate(params[:session][:password])
      log_in @user
      # チェックボックスが押してあったら記憶する
      params[:session][:remember_me] == '1' ? remember(@user) : forget(@user)
      redirect_back_or @user
    else
      flash.now[:danger] = 'Invalid email/password combination'
      render 'new'
    end
  end

  # ログアウトする
  # logged_in?がtrueの場合のみlog_outを呼び出す
  def destroy
    log_out if logged_in?
    redirect_to root_url
  end
end